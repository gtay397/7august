"use strict";

// Provided variables.
var code = "M";

// Variables you'll be assigning to in this question.
var gender;

// TODO Your code for part (1) here.
if (code == "F") {
    var gender = "female";
}
else if (code == "M") {
    var gender = "male";
}
else {
    var gender = "unknown";
}

// Printing the answer
console.log ("The gender is:" + gender)
"use strict";

// Provided variables.
var year = 1988;

// Variables you'll be assigning to in this question.
var isLeapYear;

// TODO Your code for part (2) here.
if ((year % 4 == 0) && (year % 100 !== 0)) {
    var isLeapYear = true;
}
if ((year % 4 == 0) && (year % 100 == 0) && (year % 400 !== 0)) {
    var isLeapYear = false;
}
    if ((year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0)) {
        var isLeapYear = true;
    }

    else {
        var isLeapYear = false;
    }

    // Printing the answer
    if (isLeapYear = true) {
        console.log("The year " + year + " is a leap year.");
    } else if (isLeapYear = false) {
        console.log("The year " + year + " is NOT a leap year.");
    }
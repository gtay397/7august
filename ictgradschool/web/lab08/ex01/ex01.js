"use strict";

// TODO Declare variables here
var myFirstNumber = 3;
var myFirstBoolean = false;
var mySecondNumber = 10.25;
var myFirstString = "Hello";
var mySecondString = "World";
var questionSix = myFirstNumber + 7;
var questionSeven = mySecondNumber - myFirstNumber;
var questionEight = myFirstString + mySecondString;
var questionNine = myFirstString + myFirstNumber;
var questionTen = mySecondString - mySecondNumber;


// TODO Use console.log statements to print the values of the variables to the command line.
console.log(myFirstNumber);
console.log(questionTen);
console.log(questionEight);

